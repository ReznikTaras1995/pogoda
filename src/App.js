import React from "react";
import Info from "./conponents/info"
import Form from "./conponents/form"
import Weather from "./conponents/weather"

const API_KEY = "4c6abde8388c9ada4820b520f2443840";

class App extends React.Component {

    state = {
        temp: undefined,
        city: undefined,
        country: undefined,
        pressure: undefined,
        sunrise: undefined,
        sunset: undefined,
        error: undefined
    }

    gettingWeather = async (e) => {
        e.preventDefault();
        const city = e.target.elements.city.value;

        if (city) {
            const api_url = await
                fetch(`http://api.openweathermap.org/data/2.5/weather?q=   ${city}&appid=${API_KEY}&units=metric`);
            const data = await api_url.json();
            console.log(data);

            // const api_url_7days = await
            //     fetch(`http://api.openweathermap.org/data/2.5/onecall?lat=${data.coord.lat}&lon=${data.coord.lon}&appid=${API_KEY}&units=metric`);
            // const data_7days = await api_url_7days.json();
            // console.log(data_7days);

            let sunset = data.sys.sunset;
            let date = new Date();
            date.setTime(sunset);
            let sunset_date = date.getHours() + ":" + date.getSeconds();

            this.setState({
                temp: data.main.temp,
                city: data.name,
                country: data.sys.country,
                pressure: data.main.pressure,
                sunrise: data.sys.sunrise,
                sunset: sunset_date,//data.sys.sunset,
                error: undefined
            });
        } else {
            this.setState(
                {
                    temp: undefined,
                    city: undefined,
                    country: undefined,
                    pressure: undefined,
                    sunrise: undefined,
                    sunset: undefined,
                    error: "Ведите название города"
                });
        }
    }

    render() {
        return (
            <div className={"wrapper"}>
                <div className="main">
                    <div className="container">
                        <div className="row">
                            <div className={"col-sm-5 info"}><Info/></div>
                            <div className={"col-sm-7 form"}>
                                <Form weatherMethod={this.gettingWeather}/>
                                <Weather
                                    temp={this.state.temp}
                                    city={this.state.city}
                                    country={this.state.country}
                                    pressure={this.state.pressure}
                                    sunrise={this.state.sunrise}
                                    sunset={this.state.sunset}
                                    error={this.state.error}
                                />
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        );
    }
}

export default App;